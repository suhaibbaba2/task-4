<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();


Route::get('test',"HomeController@index")->name('test');



Route::get('/home', 'MovieController@index')->name('home');
Route::get('/insert', 'MovieController@Insert');
Route::get('/postChangeLanguage/{local}','HomeController@postChangeLanguage')->name('postChangeLanguage');

Route::prefix('movies')->group(function (){

    /**
     * Show all Movies
     */
    Route::get('/','MovieController@allMovies')->name('allMovies');

    /**
     * Show movie by Id
     */
    Route::get('/show/{id}','MovieController@showMovie')->name('showMovie');

    Route::post('/create','MovieController@insertMovie')->name('insertMovie');

    Route::get('/create','MovieController@insertMovieView')->name('addMovie');

    Route::post('/search','MovieController@searchMovies')->name('searchMovies');

    Route::get('/delete/{id}','MovieController@deleteMovie')->name('deleteMovie');

    Route::post('/update','MovieController@updateMovie')->name('updateMovie');
    Route::get('/edit/{id}','MovieController@editMovieView')->name('editMovie');

});

Route::prefix('update')->group(function () {

    Route::post('/rate','RatingController@rate')->name('rate');
});
Route::post('/update/updateRating','RatingController@updateRating')->name('updateRating');

Route::get('/test',"MovieController@test_insert");