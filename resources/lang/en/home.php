<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/9/2017
 * Time: 3:21 PM
 */

return [
    "movies" =>"Movies",
    "new_movie" =>"New Movie",
    "register"  => "Register",
    "login"     => "Login",
    "show_more" => "Show More",
    "new_movies"=> "New Movies",
    "read_more" => "Read More",
];