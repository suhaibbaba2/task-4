<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/10/2017
 * Time: 1:21 AM
 */

return [
    "name"      => "Name",
    "password"    => "Password",
    "email" => "Email ",
    "confirm_password"=> "Confirm Password",
    "remember_me"=> "Remember Me",
    "login"=> "Login",
    "register"=> "Register",
    "logout"=> "Logout",
    "forgot_your_password"=> "Forgot Your Password",
];