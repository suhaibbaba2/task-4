<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/9/2017
 * Time: 3:21 PM
 */

return [
    "movies" =>"افلام",
    "new_movie" =>"فيلم جديد",
    "register"  => "حساب جديد",
    "login"     => "تسجيل دخول",
    "show_more" => "المزيد",
    "new_movies"=> "أفلام جديدة",
    "read_more" => "المزيد",
];