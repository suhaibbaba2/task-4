<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/10/2017
 * Time: 1:21 AM
 */

return [
    "name"      => "الاسم",
    "password"    => "كلمة السر",
    "email" => "email ",
    "confirm_password"=> "تاكيد كلمة المرور",
    "remember_me"=> "تذكر كلمة المرور",
    "login"=> "تسجيل دخول",
    "register"=> "حساب جديد",
    "logout"=> "تسجيل خروج",
    "forgot_your_password"=> "هل نسيت كلمة المرور",

];