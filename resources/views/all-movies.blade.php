@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <?php $i = 0?>
            @if(count($movies)<1)
                <h2 class="text-center" style="font-weight: 800">No Data</h2>
            @endif
            @foreach($movies as $movie)
                <div class=" col-sm-4 col-lg-4">

                    <div class="panel panel-primary">
                        <div class="panel-heading text-center"><span
                                    style="text-transform: capitalize">{{$movie->name}}</span></div>
                        <div class="movies-show panel-body">
                            <a href="#">
                                <div class="star-div-show-all-movies">
                                    <div class="star-div-include">
                                        <span class="star-text">{{$rating[$i]}}</span>
                                        <i class="glyphicon glyphicon-star star-icon"></i>
                                    </div>
                                </div>
                                <a href="{{route('showMovie',['id'=>$movie->id])}}">
                                    <img src="{{upload_file_path($movie->poster)}}"
                                         class="movies-img-show" style="width:100%;height: 320px" alt="{{$movie->poster}}">
                                </a>
                            </a>
                        </div>
                        <div class="panel-footer"><i class="glyphicon glyphicon-eye-open"></i> {{$watching[$i]}}</div>
                        <?php $i++?>
                    </div>
                </div>
            @endforeach

        </div>

        <div class="row text-center" style="margin-top: 80px;margin-bottom: 80px">
            {{$movies->links()}}
        </div>


    </div>

@endsection