@extends('layouts.app')

@section('content')

    <style>
        .create-movie input {
            margin-bottom: 20px;
        }

        #div_new_poster, #div_new_trailer {
            display: none;
        }
    </style>
    <div class="container">
        <div class="page-header" style="border-bottom: 1px solid #500808">
            <h3 style="text-indent: 10px">Edit Movie : {{$movie->name}}</h3>
        </div>
        <div class="row create-movie" style="padding-left: 20px">
            <form action="{{route('updateMovie')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$movie->id}}">
                <div class="col-lg-6">
                    <input type="text" class="form-control" name="name" placeholder="Name" value="{{$movie->name}}">
                    <textarea class="form-control" name="description"
                              placeholder="Description" rows="8">{{trim($movie->description)}}</textarea>

                    <div id="div_old_poster" class="div_poster" style="margin: 20px 0">
                        <label for="poster">Poster</label>
                        <button type=button onclick="removeOldValue('div_poster')" class="btn"
                                style="border: none;background:none;">
                            <i class="glyphicon glyphicon-remove" style="color: #d9534f;font-size: 20px"></i>
                        </button>
                        <br>
                        <img src="{{upload_file_path($movie->poster)}}" width="100" height="100">

                    </div>
                    <div id="div_new_poster" class="div_poster" style="margin-top: 20px">
                        <label for="poster">Poster</label>
                        <input type="file" class="form-control" name="poster" placeholder="Poster" accept="image/*">
                    </div>

                    <div id="div_old_trailer" class="div_trailer" style="margin: 20px 0">
                        <label for="trailer">Trailer</label>
                        <button type=button onclick="removeOldValue('div_trailer')" class="btn"
                                style="border: none;background:none;">
                            <i class="glyphicon glyphicon-remove" style="color: #d9534f;font-size: 20px"></i>
                        </button>
                        <br>
                        <video width="320px" height="200px" poster="{{upload_file_path($movie->poster)}}"
                               controls>
                            <source src="{{upload_file_path($movie->trailer)}}" type="video/mp4">
                            <source src="movie.ogg" type="video/ogg">
                            Your browser does not support the video tag.
                        </video>
                    </div>
                    <div id="div_new_trailer" class="div_trailer" style="margin-top: 20px">
                        <label for="trailer">Trailer</label>
                        <input type="file" class="form-control" name="trailer" placeholder="trailer" accept="video/mp4">
                    </div>

                    <input type="submit" class="btn btn-success text-center center-block" name="submit"
                           value="Edit Movie">
                </div>
            </form>
        </div>
    </div>

    <script>
        function removeOldValue(id) {
            $('.' + id).fadeToggle();
        }
    </script>
@endsection