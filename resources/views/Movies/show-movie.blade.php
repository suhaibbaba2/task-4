@extends('layouts.app')

@section('style')
    <link href="{{ asset('css/show_event.css') }}" rel="stylesheet">
@endsection

@section('content')


    <div class="container-fluid">

        @if(count($errors->errorlogin)>0)
            <div class="container row">
                <div class="alert alert-danger center-block">
                    <strong>Error!</strong> {{$errors->errorlogin->first()}}
                </div>
            </div>
        @endif

        <div class="row content show-movie">

            <div class="col-sm-3 sidenav ">
                <h3 id="show-title" class="text-center">{{$movie->name}}</h3>
                <div>
                    <img src="{{upload_file_path($movie->poster)}}" height="350" width="100%" alt="{{$movie->poster}}">
                </div>

                <div class="text-center">
                    <label for="rate">Rate :</label>
                    <div id="rate" class="star-div-include">
                        <span style="color: #ffffff"><span class="star-text"><span id="rate_text">{{$rating}}</span> / </span>5</span>
                        <i class="glyphicon glyphicon-star star-icon"></i>
                    </div>
                    <label>Watching :</label>
                    <div class="star-div-include watch-div">
                        <span id="watch_text" class="star-text">{{$watching}}</span>
                        <i class="glyphicon glyphicon-eye-open watch-icon"></i>
                    </div>
                </div>
                <div>
                    <form style="margin-left: 80px">
                        <fieldset>
                        <span class="star-cb-group">
                          <input type="radio" id="rating-5" name="rating" value="5"/><label for="rating-5">5</label>
                          <input type="radio" id="rating-4" name="rating" value="4" checked="checked"/><label
                                    for="rating-4">4</label>
                          <input type="radio" id="rating-3" name="rating" value="3"/><label for="rating-3">3</label>
                          <input type="radio" id="rating-2" name="rating" value="2"/><label for="rating-2">2</label>
                          <input type="radio" id="rating-1" name="rating" value="1"/><label for="rating-1">1</label>
                          <input type="radio" id="rating-0" name="rating" value="0" class="star-cb-clear"/><label
                                    for="rating-0">0</label>
                        </span>
                        </fieldset>
                    </form>

                </div>
            </div>


            <div class="col-sm-8">

                @if(Auth::check())
                    <div class="col-lg-6 pull-right text-right">
                        <a href="{{route('editMovie',['id'=>$movie->id])}}" class="btn btn-warning"
                           style="margin-right: 20px;"><i
                                    class="glyphicon glyphicon glyphicon-pencil"></i>&nbsp;{{Lang::get('show_movies.edit')}}
                        </a>

                        @if(Auth::id()==$movie->created_by)
                            <a href="{{route('deleteMovie',['id'=>$movie->id])}}" class="btn btn-danger"><i
                                        class="glyphicon glyphicon-remove"></i>&nbsp;{{Lang::get('show_movies.delete')}}
                            </a>
                        @endif

                    </div>
                @endif
                <h2 class="show-title">{{Lang::get('show_movies.description')}}</h2>
                <h5><span class="glyphicon glyphicon-time post-by"></span> Post by <span
                            style="text-transform: capitalize"><strong>{{$created_by_name}}</strong></span>, {{$movie->created_at->format('D.M.Y')}}
                </h5>
                <p id="show-description">
                    {{$movie->description}}
                </p>
                <br><br>

                <hr>
                <div style="margin: 60px 10px;">
                    <video width="100%" height="500px" poster="{{upload_file_path($movie->poster)}}"
                           controls>
                        <source src="{{upload_file_path($movie->trailer)}}" type="video/mp4">
                        <source src="movie.ogg" type="video/ogg">
                        Your browser does not support the video tag.
                    </video>
                </div>
                <hr>

                <div class="col-lg-12 center-block text-center" style="margin-top: 80px;margin-bottom: 50px">
                    @if(count($images)>0)
                        {{--Images Upload--}}
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                @for($i=0;$i<count($images);$i++)
                                    @if($i==0)
                                        <li data-target="#myCarousel" data-slide-to="{{$i}}" class="active"></li>
                                    @else
                                        <li data-target="#myCarousel" data-slide-to="{{$i}}"></li>
                                    @endif
                                @endfor
                            </ol>
                            <!-- slides -->
                            <div class="carousel-inner">
                                <?php $item_is_active = true;?>
                                @foreach($images as $item)
                                    @if($item_is_active)
                                        <div class="item active">
                                            <?php $item_is_active = false;?>
                                            @else
                                                <div class="item">
                                                    @endif
                                                    <img class="center-block"
                                                         src="{{upload_file_path($item->image)}}"
                                                         alt="{{$item->poster}}">
                                                </div>
                                            @endforeach

                                            <!-- Left and right controls -->
                                                <a class="left carousel-control" href="#myCarousel" data-slide="prev"
                                                   style="background-image:none ">
                                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="right carousel-control" href="#myCarousel" data-slide="next"
                                                   style="background-image:none ">
                                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                        </div>
                            </div>

                        </div>
                    @endif
                </div>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body text-center">

                    <div class="social-media center-block">
                        <a href="{{URL::to('/')}}/login">
                            <div class="well">
                                <span style="color: #FFF;font-size: 20px">Login</span>
                            </div>
                        </a>

                        <a href="{{URL::to('/')}}/register">
                            <div class="well">
                                <span style="color: #FFF;font-size: 20px">Register</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        var url_update_rate = "{{URL::to('/')}}/update/updateRating";

        var logID = 'log',
            log = $('<div id="' + logID + '"></div>');
        $('body').append(log);
        $('[type*="radio"]').change(function () {
            var me = $(this);
            var rate = me.attr('value');

            var data = {
                "movie_id": "{{$movie->id}}",
                "users_id": "{{Auth::id()}}",
                "rate": rate
            }

            var url = "{{URL::to('/')}}/update/rate"
            sendAjax(data, url)

        });

        $("#rating-{{"$rating"}}").prop('checked', true);

        function sendAjax(data, url) {
            if ("{{!Auth::check()}}") {
                $('#myModal').modal('show')
            } else {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: url,
                    method: "POST",
                    data: data,
                    success: function (result) {
                        UpdateRating(data)

                    }
                });

            }
        }//ajax

        function UpdateRating(data) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: url_update_rate,
                method: "POST",
                data: data,
                success: function (json) {
                    var result = JSON.parse(json);

                    $('#watch_text').text(result.watch);
                    $('#rate_text').text(result.rate)
                }
            });


        }//ajax

    </script>

@endsection