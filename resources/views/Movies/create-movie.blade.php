@extends('layouts.app')

@section('content')

    <style>
        .create-movie input {
            margin-bottom: 20px;
        }
    </style>

    @foreach($errors->all() as $error)
        <div class="container row center-block">
            <div class="alert alert-danger center-block">
                <strong>Error!</strong> {{$error}}
            </div>
        </div>
    @endforeach
    <div class="container">
        <div class="page-header" style="border-bottom: 1px solid #500808">
            <h3 style="text-indent: 10px">{{Lang::get('create.add_movie')}}</h3>
        </div>
        <div class="row create-movie" style="padding-left: 20px">
            <form action="{{route('insertMovie')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="col-lg-6">
                    <input type="text" class="form-control" name="name" placeholder="{{Lang::get('create.name')}}" required>
                    <textarea class="form-control" name="description" placeholder="{{Lang::get('create.description')}}" required></textarea>
                    <label for="poster">{{Lang::get('create.poster')}}</label>
                    <input type="file" class="form-control" name="poster"
                           placeholder="{{Lang::get('create.poster')}}" accept="image/*"  required>
                    <label for="poster">{{Lang::get('create.trailer')}}</label>
                    <input type="file" class="form-control" name="trailer" placeholder="trailer" accept="video/mp4" required>
                    <input type="submit" class="btn btn-success text-center center-block" name="submit"
                           value="Add Movie">
                </div>
            </form>
        </div>
    </div>

@endsection