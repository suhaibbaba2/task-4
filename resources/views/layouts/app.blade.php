<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Movies</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    @if(\Illuminate\Support\Facades\App::getLocale()=="ar")
        <link href="{{ asset('css/bootstrap_rtl.css') }}" rel="stylesheet">
    @endif
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

@yield('style')

<!-- Scripts -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    @yield('script')

    <style>
        .search-box {
            display: none;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            right: 0;
            background-color: #f9f9f9;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(255, 0, 0, 0.33);
            z-index: 1;
            top: 34px;
            padding-right: 0px;
            padding-left: 0px;

        }

        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {
            background-color: #c8c8c8

        }
    </style>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-inverse navbar-static-top" style="color:#ffffff !important;">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{Lang::get('home.movies')}}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <li><a id="search-button" onclick="ShowSearchBox()" class="glyphicon glyphicon-search"
                           style="cursor: pointer;font-size: 20px"></a></li>

                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ route('login') }}">{{Lang::get('home.login')}}</a></li>
                        <li>
                            <a href="{{ route('register') }}">{{Lang::get('home.register')}}</a>
                        </li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><span style="text-transform: capitalize">
                                {{ Auth::user()->name }} </span><span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ route('addMovie') }}"><i
                                                class="glyphicon glyphicon-plus"></i> {{Lang::get('home.new_movie')}}
                                    </a></li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i
                                                class="glyphicon glyphicon-log-out"></i>
                                        {{Lang::get('auth_login.logout')}}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                    @if(App::getLocale()=="en")
                        <li><a href="{{route("postChangeLanguage",['lang'=>'ar'])}}">Arabic</a></li>
                    @else
                        <li><a href="{{route("postChangeLanguage",['lang'=>'en'])}}">English</a></li>
                    @endif


                </ul>
            </div>
        </div>
    </nav>

    <div class="container center-block" style="margin: 0 auto;">
        <div class="search-box row">
            <div class="dropdown col-lg-12">
                <div class="form-group has-feedback">
                    <input id="search" type="search"
                           class="dropbtn form-control"
                           onkeyup="Search(this.value , '{{route('searchMovies')}}','{{route('showMovie',['id' => null])}}')"
                           placeholder="Search" style="margin-bottom: 40px;    padding-left: 35px;">
                    <span class="glyphicon glyphicon-search form-control-feedback" style="left: 0;color: #ccc"></span>
                </div>
                <div id="livesearch" class="dropdown-content col-lg-11" style="left: 16px !important;">
                </div>

            </div>

        </div>

    </div>

    @yield('content')
</div>
<script>
    //    ShowSearchBox();
</script>
</body>
</html>
