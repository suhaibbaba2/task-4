@extends('layouts.app')

@section('content')
    <div class="container-fluid home text-center">
        <div class="row">

            <div class="col-sm-3 well">

                <div class="well text-center" style="background: #f0ad4e;    color: #634646;">
                    <p><i class="glyphicon glyphicon-film" style="font-size:30px"></i></p>
                    <p style="font-size:30px;font-weight: 600;">{{Lang::get('home.new_movies')}}</p>
                </div>

                @if(count($array_higher_watching['higher_watching'])>0&& count($array_higher_watching['higher_watching_rating'])>0)
                    <div class="well">
                        <p>
                            <a href="{{route("showMovie",["id"=> $array_higher_watching['higher_watching']->id])}}">{{$array_higher_watching['higher_watching']->name}}</a>
                        </p>
                        <video width="100%" height="210px"
                               poster="{{upload_file_path($array_higher_watching['higher_watching']->poster)}}"
                               controls>
                            <source src="{{upload_file_path($array_higher_watching['higher_watching']->trailer)}}"
                                    type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                        <div class="star-div-include" style="margin-top: 20px">
                            <span style="color: #ffffff"><span
                                        class="star-text">{{$array_higher_watching['higher_watching_rating']}}</span> /5</span>
                            <i class="glyphicon glyphicon-star star-icon"></i>
                        </div>
                        <div class="star-div-include watch-div" style="margin-top: 20px">
                            <span class="star-text">{{$array_higher_watching['higher_watching_watching']}}</span>
                            <i class="glyphicon glyphicon-eye-open watch-icon"></i>
                        </div>
                    </div>
                @endif

            </div>

            <div class="col-sm-7">

                {{--Videos--}}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default text-left" style="height: 255px;">
                            <div class="panel-body" style="padding: 0px;">

                                @foreach($trailers as $item)
                                    <div class="col-lg-6" style="padding-top: 20px">
                                        <video width="100%" height="210px"
                                               poster="{{upload_file_path($item->poster)}}" controls>
                                            <source src="{{upload_file_path($item->trailer)}}"
                                                    type="video/mp4">
                                        </video>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>

                {{--Videos and Description--}}
                <?php $i = 0;?>
                @foreach($last_three_movies['last_movies'] as $item)
                    <div class="row ">
                        <div class="col-sm-5">
                            <div class="well">
                                <p>{{$item->name}}</p>
                                <div class="star-div">
                                    <div class="star-div-include">
                                        <span class="star-text">{{$last_three_movies['rating_avg'][$i]}}</span>
                                        <i class="glyphicon glyphicon-star star-icon"></i>
                                    </div>
                                </div>
                                <img src="{{upload_file_path($item->poster)}}" height="140" width="100%"
                                     alt="{{$item->name}}">
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="well description-box">
                                <p>{{substr($item->description, 0, 400)}}</p>
                                <p class="read-more">
                                    <a href="{{route("showMovie",["id"=> $item->id])}}" class="button">{{Lang::get('home.read_more')}}</a>
                                </p>

                            </div>
                        </div>
                    </div>
                    <?php $i++?>
                @endforeach
                <a href="{{route("allMovies")}}" id="show_more" class="btn btn-default">{{Lang::get('home.show_more')}}</a>
            </div>

            <div class="col-sm-2 well">
                @if(count($random_movie['movie'])>0)
                    <div class="thumbnail">
                        <a href="{{route("showMovie",["id"=>$random_movie['movie']->id])}}" class="button">
                            <p>{{$random_movie['movie']->name}}</p></a>
                        <img src="{{upload_file_path($random_movie['movie']->poster)}}"
                             alt="{{upload_file_path($random_movie['movie']->name)}}" width="400" height="300">
                        @if(count($random_movie['rating'])>0)
                            <div class="star-div-include" style="margin-top: 20px">
                                <span class="star-text">{{$random_movie['rating']}}</span>
                                <i class="glyphicon glyphicon-star star-icon"></i>
                            </div>
                        @endif
                    </div>
                @endif

                <div class="social-media">
                    <a href="#">
                        <div class="well">
                            <span style="color: #FFF;font-size: 20px">facebook</span>
                        </div>
                    </a>

                    <a href="#">
                        <div class="well">
                            <span style="color: #FFF;font-size: 20px">twitter</span>
                        </div>
                    </a>
                </div>


            </div>
        </div>
    </div>
@endsection