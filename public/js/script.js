function SetWidth() {
    var width = $("#search").css('width');
    $("#livesearch").css('width', width)
}

$(document).click(function (e) {

    if (e.target.id == 'search') {
        $("#livesearch").css('display', 'block')
    }
    else if (e.target.id != 'livesearch') {
        $("#livesearch").css('display', 'none');
    }
});

function ShowSearchBox() {
    $('.search-box').fadeToggle(4000);
}

var timeout;
function Search(str, baseUrl,url_show_movie) {

    if (timeout) {
        clearTimeout(timeout);
    }
    timeout = setTimeout(function () {

        var input = {'input': str}
        $("#livesearch").empty();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: baseUrl,
            data: input,
            type: 'POST',
            cache: false,
            success: function (data) {
                var search_output = JSON.parse(data);
                if (search_output.length < 1) {
                    var div = "<h5 class='text-center'>No Subjects Found</h5>";
                    $('#livesearch').append(div)
                } else {
                    for (var i = 0; i < search_output.length; i++) {
                        var url = url_show_movie+"/"+search_output[i]['id'];
                        var div = "<a href=" + url + " style=text-transform:capitalize>" + search_output[i]['name'] + "</a>";

                        $('#livesearch').append(div)
                    }

                }
                SetWidth()
                $(".dropdown-content").css('display', 'block')

            }//success
        });
    }, 500);
}//function
