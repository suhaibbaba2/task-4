<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function images()
    {
        return $this->hasMany(ImagesMovie::Class);
    }

    public function rating()
    {
        return $this->hasMany(Rating::Class);
    }

    public function createdBy()
    {
        return $this->hasMany(User::Class,'id','created_by')->select('name','id');
    }

}
