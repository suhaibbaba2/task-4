<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Rating extends Model
{

    public static function higherRate()
    {
//        $query ="SELECT movie_id, COUNT(movie_id) AS movie FROM ratings
//                  GROUP BY movie_id HAVING COUNT(movie_id) > 1 ORDER BY COUNT(movie_id) DESC";

        $users = DB::table('ratings')
            ->select(array('movie_id', DB::raw('COUNT(movie_id) as movie')))
            ->groupBy('movie_id')
            ->orderBy('movie', 'desc')
            ->first();

        if ($users != null) {
            return $users->movie_id;
        }
        return null;


    }
}
