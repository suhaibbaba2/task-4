<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreCreateMovie extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::check())
            return true;
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'description' => 'required',
            'poster' => 'required|mimes:jpeg,png,bmp,jpg',
            'trailer' => 'required|mimes:mp4',
//            'trailer' => 'required',
            'submit' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please Enter Movie Name',
            'description.required' => 'Please Enter Movie Description',
            'poster.required' => 'Please Enter Movie Poster',
            'trailer.required' => 'Please Enter Movie Trailer',
        ];
    }

}
