<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{

    public function index(){
        $lang=App::getLocale();
        echo $lang;
        return view("test");
    }

    public function postChangeLanguage($lang){
        App::setLocale($lang);
        $lang=App::getLocale();
        Session::put('lang',$lang);
        Session::save();

        return redirect()->back();
    }


}
