<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/5/2017
 * Time: 2:15 PM
 */

namespace App\Http\Controllers;


use App\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    function rate()
    {
        $rating_model = Rating::where("movie_id", request()->movie_id)->where("users_id", request()->users_id)->first();

//        This mean that the user is rating this movie Before this time
        if ($rating_model != null) {
            $rate = Rating::find($rating_model->id);
            $rate->rating = request()->rate;
            $rate->save();
        } else {
            $rate = new Rating;
            $rate->rating = request()->rate;
            $rate->movie_id = request()->movie_id;
            $rate->users_id = request()->users_id;
            $rate->save();

        }

        print_r(request()->all());
    }


    function updateRating()
    {
        $rating_model = Rating::where("movie_id", request()->movie_id)->where("users_id", request()->users_id)->get();
        $rate = avgRating($rating_model);
        $watch = countRating($rating_model);

        echo json_encode(['watch' => $watch, "rate" => $rate]);
    }


}