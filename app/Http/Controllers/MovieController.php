<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCreateMovie;
use App\ImagesMovie;
use App\Movie;
use App\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class MovieController extends Controller
{
    public function index()
    {

        $data = [];
        $movies_trailers = Movie::select(['id', 'name', 'poster', 'trailer'])->orderBy('id', 'desc')->take(2)->get();
        $last_movies = Movie::select(['id', 'name', 'poster', 'description'])->orderBy('id', 'desc')->take(3)->get();

        foreach ($last_movies as $item) {
//            $item->description = substr($item->description, 0, 400);
            $item->description = $item->description;
        }


        $higher_watching_id = Rating::higherRate();
        if (!empty($higher_watching_id)) {
            $higher_watching = Movie::find($higher_watching_id)->select(['id', 'name', 'poster', 'trailer'])->first();
        }
        //higher watching
        $higher_watching_rating = 0;
        $higher_watching_watching = 0;
        if (!empty($higher_watching)) {
            $ratings_model = Movie::find($higher_watching_id)->rating;
            if (!empty($ratings_model)) {

                $higher_watching_rating = avgRating($ratings_model);
                $higher_watching_watching = countRating($ratings_model);

            }

        }

        //Rate for last 3 movies
        $rating_avg = [];
        foreach ($last_movies as $item) {
            $ratings_model = Movie::find($item->id)->rating;
            $rating = avgRating($ratings_model);
            array_push($rating_avg, $rating);
        }

        //Random Movie
        $random_movie = Movie::inRandomOrder()->first();
        if (!empty($random_movie)) {
            $ratings_random_movie_model = Movie::find($random_movie->id)->rating;
            $rating_random_movie = avgRating($ratings_random_movie_model);
        }


        $data['trailers'] = $movies_trailers;
        $data['last_three_movies'] = ["last_movies" => $last_movies, "rating_avg" => $rating_avg];
        $data['array_higher_watching'] =
            ["higher_watching" => !empty($higher_watching) ? $higher_watching : null,
                "higher_watching_watching" => !empty($higher_watching_watching) ? $higher_watching_watching : null,
                "higher_watching_rating" => !empty($higher_watching_rating) ? $higher_watching_rating : null,];
        $data['random_movie'] = ["movie" => !empty($random_movie) ? $random_movie : null, "rating" => !empty($rating_random_movie) ? $rating_random_movie : null];



        return view('home', $data);
    }

    /**
     * Show All Movies
     * -> every page contain 20 item only
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allMovies()
    {

        $movies = Movie::select(['id', 'name', 'poster'])->orderBy('id', 'desc')->paginate(20);
        $rating_avg = [];
        $watching_avg = [];

        foreach ($movies as $item) {
            $ratings_model = Movie::find($item->id)->rating;
            $rating = avgRating($ratings_model);
            $watching = countRating($ratings_model);
            array_push($rating_avg, $rating);
            array_push($watching_avg, $watching);
        }

        return view('all-movies', array('movies' => $movies, 'rating' => $rating_avg, 'watching' => $watching_avg));
    }

    public function showMovie($id = null)
    {
        $movie = Movie::find($id);
        if(!$movie) return redirect()->route('home');
        $ratings_model = Movie::find($id)->rating;
        $rating = avgRating($ratings_model);
        $watching = countRating($ratings_model);
        $created_by_array = Movie::find($id)->createdBy()->first();

        $images=Movie::find($id)->images;

        return view('Movies.show-movie',
            array(
                'movie' => $movie,
                'rating' => $rating,
                'watching' => $watching,
                'created_by_name' => $created_by_array['name'],
                'images' => $images,
            )
        );
    }

    public function searchMovies()
    {
        $search_movie = Movie::select('name', 'id')->where('name', 'LIKE', "%".request()->input."%")->get();
        echo json_encode($search_movie);
    }

    public function deleteMovie($id = null)
    {

        $deleteMovie = Movie::find($id);

        if (Auth::id() == $deleteMovie->created_by) {
            $deleteMovie->delete();
        }
        return redirect()->route('allMovies');
    }

    public function updateMovie()
    {
        $request = request();

        $movie = [];
        $movie['name'] = $request->name;
        $movie['description'] = $request->description;
        $movie['updated_by'] = Auth::id();

        if($request->hasFile('poster')) {
            $file_name = pathinfo($request->file('poster')->getClientOriginalName(), PATHINFO_FILENAME);
            $poster = $file_name . "." . $request->file('poster')->extension();
            $movie['poster'] = $poster;
            $request->file('poster')->storeAs('', $poster);

        }
        if($request->hasFile('trailer')) {
            $file_name = pathinfo($request->file('trailer')->getClientOriginalName(), PATHINFO_FILENAME);
            $movie['trailer'] = $file_name . ".mp4";
            $request->file('poster')->storeAs('', $movie['trailer']);
        }

        Movie::where('id', $request->id)
        ->update($movie);
        return redirect()->route('allMovies');
    }

    public function insertMovieView()
    {
        if (Auth::check())
            return view('Movies.create-movie');
        return redirect()->route('allMovies');
    }

    public function editMovieView($id=null)
    {
        if (Auth::check()) {
            $movie=Movie::find($id);
            return view('Movies.edit-movie',array('movie'=>$movie));
        }
        return redirect()->back()->withErrors("Please Login or Register","errorlogin");
    }

    public function insertMovie(StoreCreateMovie $request)
    {
        $file_name = pathinfo($request->file('poster')->getClientOriginalName(), PATHINFO_FILENAME);
        $poster = $file_name . "." . $request->file('poster')->extension();
        $movie = new Movie;
        $movie->name = $request->name;
        $movie->description = $request->description;
        $movie->created_by = Auth::id();
        $movie->updated_by = Auth::id();

        $movie->poster = $poster;
        $movie->trailer = $file_name . ".mp4";

        $request->file('poster')->storeAs('', $poster);

        $movie->save();
        return redirect()->route('allMovies');

    }

}
