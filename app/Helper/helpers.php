<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/11/2017
 * Time: 12:33 PM
 */


function upload_file_path($name = '')
{
    return asset('upload_file/') . "/" . $name;

}


function avgRating($rates=[])
{
    $rate_avg = 0.0;
    $length = 0;
    foreach ($rates as $item) {
        $rate_avg += $item->rating;
        $length += 1;
    }
    if ($length == 0)
        return $rate_avg;
    return $rate_avg / $length;
}


function countRating($rates = [])
{
    $length = 0;
    foreach ($rates as $item) {
        $length += 1;
    }
    return $length;
}
