<?php

use Illuminate\Database\Seeder;

class movieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<100;$i++) {
            DB::table('movies')->insert([
                'name' => str_random(10),
                'description' => str_random(400),
                'poster' => "OmryEbtada.jpg",
                'trailer' => "OmryEbtada.mp4",
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => new  DateTime(),
                'updated_at' => new DateTime(),
            ]);

        }
    }
}
